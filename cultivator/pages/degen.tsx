import type { InferGetServerSidePropsType } from "next/types"

export const getServerSideProps = async () => ({
  props: {
    subscribe: 'to theo'
  }
})


export default function Foo(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
  console.log({ Foo: props })
  return <div>Hello, degens</div>
}